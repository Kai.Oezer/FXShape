/**
This file is part of the FXShape project.
Copyright (C) 2021-2024 Kai Berk Oezer
https://gitlab.com/Kai.Oezer/FXShape

FXShape is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import FXShape
import SwiftUI

struct ShapeViewWrapper : NSViewRepresentable
{
	private let _shapeView = ShapeView()

	var source : String = ""

	func makeNSView(context: Context) -> FXShape.ShapeView
	{
		_shapeView
	}

	func updateNSView(_ view : FXShape.ShapeView, context: Context)
	{
		view.shape = _shape(from: source.trimmingCharacters(in: .whitespacesAndNewlines))
		view.setNeedsDisplay(view.bounds)
	}

	private func _shape(from shapeDescription : String) -> FXShape.Shape?
	{
		guard shapeDescription.count > 3 else { return nil }
		var paths = [FXShape.Path]()
		var circles = [FXShape.Circle]()
		shapeDescription.enumerateLines { line, stop in
			let path = Path(data: line)
			if !path.segments.isEmpty
			{
				paths.append(path)
			}
			else
			{
				if let circle = _circle(from: line)
				{
					circles.append(circle)
				}
			}
		}
		guard !paths.isEmpty || !circles.isEmpty else { return nil }
		return ShapeFactory.makeShape(paths:paths, circles:circles, connectionPoints:[], size:CGSize(width:64, height:64))
	}

	private func _circle(from description : String) -> FXShape.Circle?
	{
		let CircleCommandPrefix = "circle "
		guard description.lowercased().starts(with: CircleCommandPrefix) else { return nil }
		let argumentsString = description[description.index(description.startIndex, offsetBy: CircleCommandPrefix.count)...]
		let components = argumentsString.components(separatedBy: .whitespaces)
		guard components.count > 2 else { return nil }
		let center = CGPoint(x: CGFloat(Float(components[0]) ?? 0), y: CGFloat(Float(components[1]) ?? 0))
		guard let radius = Float(components[2]), radius > 0.0 else { return nil }
		return FXShape.Circle(center: center, radius: CGFloat(radius))
	}

}
