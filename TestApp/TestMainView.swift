/**
This file is part of the FXShape project.
Copyright (C) 2021 Kai Berk Oezer
https://gitlab.com/Kai.Oezer/FXShape

FXShape is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import SwiftUI
import FXShape

struct TestMainView: View
{
	@AppStorage("ShapeSource")
	var shapeSource : String = ""

	var body: some View
	{
		VStack(alignment: .center, spacing: 10)
		{
			ShapeViewWrapper(source: shapeSource)
				.frame(height:100)
				.background(Color(.sRGB, red: 0.95, green: 1.0, blue: 0.95, opacity: 1.0))
			Text("Enter path and circle commands below.\nClose paths with a terminating 'z'.")
				.multilineTextAlignment(.center)
			TextEditor(text: $shapeSource)
				.border(Color(.sRGB, white: 0.8, opacity: 1.0), width: 1)
		}
		.padding(10)
		.background(Color(.sRGB, white: 0.92, opacity: 1.0))
	}
}

struct ContentView_Previews: PreviewProvider
{
	static var previews: some View
	{
		TestMainView()
			.previewLayout(.fixed(width: 300, height: 400))
	}
}
