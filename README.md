# FXShape

This repository contains the shape rendering library used by [Volta](https://gitlab.com/Kai.Oezer/Volta).
It is a stand-alone Xcode project with a macOS framework target and a test app target.

The original Objective-C code is re-implemented in Swift.

## Cached Rendering

FXShape is designed for fast rendering of a large number of similar shapes in different
locations on the canvas. When a shape is rendered for the first time, the lower-level
Core Graphics entities that are generated for rendering the shape are cached. When an
identical shape needs to be rendered, the cached Core Graphics entities are used.

## Dependencies

FXShape uses the double-ended queue (Deque) data structure from [Swift Collections](https://github.com/apple/swift-collections.git).

## Test App

In the test app, enter path and circle data lines by line into the text field at the bottom. If the data is valid, the shape will appear in the shape viewer at the top. The shape is updated as you make changes in the text editor.

For example, enter
```
m -10 20 l 10 10 l 20 5 
circle 4 6 10
```
to see a circle and a two-segmented line.
