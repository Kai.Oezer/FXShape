/**
This file is part of the FXShape project.
Copyright (C) 2021 Kai Berk Oezer
https://gitlab.com/Kai.Oezer/FXShape

FXShape is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import Foundation
import CoreGraphics

struct PathInfo
{
	var path : CGPath
	var filled = false
	var closed = false
}

typealias ShapeRenderData = [PathInfo]

class ShapeRenderDataPool
{
	init()
	{

	}

	deinit
	{
	}

	subscript(shape : AnyShape) -> ShapeRenderData?
	{
		if let entry = _acceleratorMap[shape]
		{
			return entry
		}
		else
		{
			let renderData = _createRenderData(for: shape)
			_acceleratorMap[shape] = renderData
			return renderData
		}
	}

	private var _acceleratorMap = [AnyShape : ShapeRenderData]()

	private func _createRenderData(for shape : AnyShape) -> ShapeRenderData
	{
		guard let basicShape = shape.shape as? BasicShape else { return [] }
		return _renderData(for: basicShape.paths) + _renderData(for: basicShape.circles)
	}

	private func _renderData(for paths : [Path]) -> ShapeRenderData
	{
		paths.map { path in
			let cpPath = CGMutablePath()
			var lastPosition = Vector2D()
			for segment in path.segments
			{
				_addCommandsForSegment(segment, to:cpPath, from: &lastPosition)
			}
			if path.closed
			{
				cpPath.closeSubpath()
			}
			return PathInfo(path: cpPath, filled: path.filled, closed: path.closed)
		}
	}

	private func _renderData(for circles : [Circle]) -> ShapeRenderData
	{
		circles.map { circle in
			let cgPath = CGMutablePath()
			let c = circle.center
			let r = circle.radius
			cgPath.addEllipse(in: CGRect(x: c.x - r, y: c.y - r, width: 2 * r, height: 2 * r))
			return PathInfo(path: cgPath, filled: circle.filled, closed: false)
		}
	}

	private func _addCommandsForSegment(_ segment : Segment, to path : CGMutablePath, from lastPosition : inout Vector2D)
	{
		switch segment
		{
			case .jump(let jumpSegment):
				_addCommandsForJumpSegment(jumpSegment, to: path, lastPosition: &lastPosition)
			case .line(let lineSegment):
				_addCommandsForLineSegment(lineSegment, to: path, lastPosition: &lastPosition)
			case .verticalLine(let vLineSegment):
				_addCommandsForVerticalLineSegment(vLineSegment, to: path, lastPosition: &lastPosition)
			case .horizontalLine(let hLineSegment):
				_addCommandsForHorizontalLineSegment(hLineSegment, to: path, lastPosition: &lastPosition)
			case .arc(let arcSegment):
				_addCommandsForArcSegment(arcSegment, to: path, lastPosition: &lastPosition)
			case .curve(let curveSegment):
				_addCommandsForCurveSegment(curveSegment, to: path, lastPosition: &lastPosition)
		}
	}

	private func _addCommandsForJumpSegment(_ segment : JumpSegment?, to path : CGMutablePath, lastPosition : inout Vector2D)
	{
		guard let segment = segment else { return }
		lastPosition = segment.isRelative ? lastPosition + segment.destination : segment.destination
		path.move(to: lastPosition.cgPoint)
	}

	private func _addCommandsForLineSegment(_ segment : LineSegment?, to path : CGMutablePath, lastPosition : inout Vector2D)
	{
		guard let segment = segment else { return }
		lastPosition = segment.isRelative ? lastPosition + segment.destination : segment.destination
		path.addLine(to: lastPosition.cgPoint)
	}

	private func _addCommandsForVerticalLineSegment(_ segment : VerticalLineSegment?, to path : CGMutablePath, lastPosition : inout Vector2D)
	{
		guard let segment = segment else { return }
		lastPosition.y = segment.isRelative ? lastPosition.y + segment.distance : segment.distance
		path.addLine(to: lastPosition.cgPoint)
	}

	private func _addCommandsForHorizontalLineSegment(_ segment : HorizontalLineSegment?, to path : CGMutablePath, lastPosition : inout Vector2D)
	{
		guard let segment = segment else { return }
		lastPosition.x = segment.isRelative ? lastPosition.x + segment.distance : segment.distance
		path.addLine(to: lastPosition.cgPoint)
	}

  /// Converts the SVG arc into a Core Graphics arc.
	///
  /// Helpful implementation notes at
  /// http://www.w3.org/TR/SVG/implnote.html#ArcConversionEndpointToCenter
  /// If one of the radii is 0 then this becomes a straight line.
	private func _addCommandsForArcSegment(_ segment : ArcSegment?, to path : CGMutablePath, lastPosition : inout Vector2D)
	{
		guard let segment = segment else { return }
		let endPosition = segment.isRelative ? lastPosition + segment.endPoint : segment.endPoint
		// If the distance between the last position and the end position is less than 5 draw a line instead.
		guard (segment.radiusX != 0.0) && (segment.radiusY != 0.0) else
		{
			lastPosition = endPosition
			path.addLine(to: lastPosition.cgPoint)
			return
		}

    // CONVERTING TO CENTER-POINT PARAMETRIZATION
    // STEP 1: Putting the origin in the middle of the final point and the end point,
    // then rotating back into the x-axis so that the current point and the end point
    // are on the x axis in the transformed space.
    var transformedLastPosition = ((lastPosition - endPosition)/2)
    transformedLastPosition.rotate(angle:-segment.rotation)
		// STEP 2: Computing the transformed center point
    let x = transformedLastPosition.x
    let y = transformedLastPosition.y
    let x2 = x * x
    let y2 = y * y
    var rx = segment.radiusX
    var ry = segment.radiusY
    var rx2 = rx * rx
    var ry2 = ry * ry
    // checking if the radii are actually large enough to reach the start point as well as the end point
		let lambda = (x2 / rx2) + (y2 / ry2)
		if lambda > 1.0
		{
			// increasing the radius
			rx2 = lambda * rx2
			ry2 = lambda * ry2
			let lambda_root = sqrt( lambda )
			rx = lambda_root * rx
			ry = lambda_root * ry
		}
    var factor = sqrt( (rx2*ry2 - rx2*y2 - ry2*x2) / (rx2*y2 + ry2*x2) )
    if segment.largeArc == segment.sweepPositive
    {
      factor = -factor
    }
    let transformedCenterPoint = Vector2D( factor * rx * y / ry, (-factor) * ry * x / rx )
    // STEP 3: Transforming the center point back into original space
    var centerPoint = transformedCenterPoint
    centerPoint.rotate(angle: segment.rotation)
    centerPoint = centerPoint + (lastPosition + endPosition)/2.0
    // STEP 4: Calculating the start angle
    let cx = transformedCenterPoint.x
    let cy = transformedCenterPoint.y
    var startAngle = Vector2D( (x - cx)/rx, (y - cy)/ry ).angle( Vector2D(1, 0) )
    if ( y < cy )
    {
      startAngle = -startAngle
    }
    // STEP 5: Calculating the sweep angle
    var sweepAngle = Vector2D( (x-cx)/rx, (y-cy)/ry ).angle( Vector2D( (-x-cx)/rx, (-y-cy)/ry ) )
    if segment.sweepPositive
    {
      while sweepAngle < 0.0 { sweepAngle += (CGFloat.pi * 2) }
    }
    else
    {
      while sweepAngle > 0.0 { sweepAngle -= (CGFloat.pi * 2) }
    }

    // We can not apply the calculated parameters 'as is' because Core Graphics accepts only one radius
    // in order to draw a circular arc instead of an elliptical arc. I therefore use the median radius.
    // So this implementation will only work correctly for circular arcs.
    path.addArc(center: centerPoint.cgPoint, radius: (rx + ry) / 2.0, startAngle: startAngle, endAngle: startAngle + sweepAngle, clockwise: sweepAngle < 0.0)
    lastPosition = endPosition;
	}

	private func _addCommandsForCurveSegment(_ segment : CurveSegment?, to path : CGMutablePath, lastPosition : inout Vector2D)
	{
		guard let segment = segment else { return }
		let cPoint1 = segment.isRelative ? lastPosition + segment.controlPoint1 : segment.controlPoint1
		let cPoint2 = segment.isRelative ? lastPosition + segment.controlPoint2 : segment.controlPoint2
		lastPosition = segment.isRelative ? lastPosition + segment.endPoint : segment.endPoint
		path.addCurve(to: lastPosition.cgPoint, control1: cPoint1.cgPoint, control2: cPoint2.cgPoint)
	}

}
