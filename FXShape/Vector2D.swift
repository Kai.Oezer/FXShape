/**
This file is part of the FXShape project.
Copyright (C) 2021 Kai Berk Oezer
https://gitlab.com/Kai.Oezer/FXShape

FXShape is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import Foundation
import CoreGraphics

public struct Vector2D : Equatable, Codable
{
	public var x : CGFloat
	public var y : CGFloat

	public init(_ x : CGFloat = 0.0, _ y : CGFloat = 0.0)
	{
		self.x = x
		self.y = y
	}

	public init(_ point : CGPoint)
	{
		x = point.x
		y = point.y
	}

	public var magnitude : CGFloat { sqrt(x * x + y * y) }

	public var squaredMagnitude : CGFloat { x * x + y * y }

	public mutating func normalize()
	{
    let mag = magnitude
    if mag > CGFloat.leastNonzeroMagnitude
    {
        x /= mag
        y /= mag
    }
    else
    {
        x = 1.0
        y = 0.0
    }
	}

	public var unitVector : Vector2D
	{
		var v = self
		v.normalize()
		return v
	}

	public var inverse : Vector2D { Vector2D(-x, -y) }

	public mutating func invert() { x = -x; y = -y }

	public func dot(_ other : Vector2D) -> CGFloat { x * other.x + y * other.y }

  /// - returns: the angle between two vectors, in radian
	public func angle(_ other : Vector2D) -> CGFloat
	{
		acos( dot(other) / other.magnitude / self.magnitude )
	}

	var cgPoint : CGPoint { CGPoint(x:self.x, y:self.y) }

  /// Rotates the vector by the given angle around the (optional) pivot point
	///
  /// - parameters:
	///   - angle: counterclockwise rotation angle in radians
  ///   - pivot: pivot point
	public mutating func rotate(angle : CGFloat, pivot : Vector2D = Vector2D(0.0, 0.0))
	{
		let cos_a = cos(angle)
		let sin_a = sin(angle)
		x -= pivot.x
		y -= pivot.y
		let xrot = (x * cos_a) - (y * sin_a)
		let yrot = (x * sin_a) + (y * cos_a)
		x = xrot + pivot.x
		y = yrot + pivot.y
	}

	public mutating func scale(_ scaleX : CGFloat, _ scaleY : CGFloat? = nil)
	{
		x = scaleX * x
		y = (scaleY ?? scaleX) * y
	}

	public static func == (lhs: Self, rhs: Self) -> Bool { (abs(lhs.x - rhs.x) < CGFloat.leastNonzeroMagnitude) && (abs(lhs.y - rhs.y) < CGFloat.leastNonzeroMagnitude) }
  public static func != (lhs: Self, rhs: Self) -> Bool { !(lhs == rhs) }
	public static func + (lhs : Vector2D , rhs : Vector2D) -> Vector2D { Vector2D(lhs.x + rhs.x, lhs.y + rhs.y) }
	public static func - (lhs : Vector2D , rhs : Vector2D) -> Vector2D { Vector2D(lhs.x - rhs.x, lhs.y - rhs.y) }
	public static func * (vec : Vector2D, f : CGFloat) -> Vector2D { Vector2D(f * vec.x, f * vec.y) }
	public static func * (f : CGFloat, vec : Vector2D) -> Vector2D { Vector2D(f * vec.x, f * vec.y) }
	public static func / (vec : Vector2D, f : CGFloat) -> Vector2D { Vector2D(vec.x / f, vec.y / f) }
}

extension Vector2D : AdditiveArithmetic
{
	public static var zero: Vector2D { Vector2D(0,0) }
	public static func += (lhs : inout Vector2D , rhs : Vector2D) { lhs.x += rhs.x; lhs.y += rhs.y }
	public static func -= (lhs : inout Vector2D , rhs : Vector2D) { lhs.x -= rhs.x; lhs.y -= rhs.y }
	public static func *= (vec : inout Vector2D, f : CGFloat) { vec.x *= f; vec.y *= f }
	public static func /= (vec : inout Vector2D, f : CGFloat) { vec.x /= f; vec.y /= f }
}

extension Vector2D : CustomStringConvertible
{
	public var description : String { "(\(x),\(y))" }
}
