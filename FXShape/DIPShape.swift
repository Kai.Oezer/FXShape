/**
This file is part of the FXShape project.
Copyright (C) 2021 Kai Berk Oezer
https://gitlab.com/Kai.Oezer/FXShape

FXShape is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import AppKit

enum DIPShapeVerticalAlignment : Int, Codable
{
	case top
	case center
	case bottom
}

enum DIPShapeHorizontalAlignment : Int, Codable
{
	case center
	case left
	case right
}

struct DIPShapeAlignment : Codable
{
	var horizontal : DIPShapeHorizontalAlignment = .center
	var vertical : DIPShapeVerticalAlignment = .center
}

fileprivate let skDIPWidth : CGFloat = 56.0;
fileprivate let skDIPMargin : CGFloat = 9.0;
fileprivate let skDIPLabelInset : CGFloat = 4.0;
fileprivate let skDIPLeadSpacing : CGFloat = 14.0;
fileprivate let skDIPLeadLength : CGFloat = 6.0;

/// The DIP label font should be monospaced and contain easily discernable characters (e.g. I and 1, O and 0).
fileprivate let skDIPTextFontName = "Menlo"

/// A shape which represents a DIP (dual in-line package)
class DIPShape : Shape
{
	public var paths : [Path] { [] }
	public var circles: [Circle] { [] }
	public var connectionPoints: [ConnectionPoint] { _leadPoints }
	public private(set) var size : CGSize = .zero
	private var _leadCount : UInt = 0
	private var _leadPoints = [ConnectionPoint]()

	public var attributes = [String : String]()
	public var doesOwnDrawing: Bool { true }
	public var isReusable : Bool { true }

	var id: String { "DIP_\(_leadCount)_\(attributes.map{"\($0.key):\($0.value)"}.joined(separator: "_"))" }

	init(leadCount : UInt)
	{
		_leadCount = ((leadCount % 2) != 0) ? (leadCount + 1) : leadCount // The lead count must be even.
		self.size = _calculateSize()
		_leadPoints = _createLeadPoints()
	}

	public func draw(context: ShapeRenderContext)
	{
		if let strokeColor = context.strokeColor
		{
			context.graphicsContext.setStrokeColor(strokeColor)
			context.graphicsContext.setFillColor(strokeColor)
		}
		_drawFrame(using: context)
		_drawLeads(using: context)
		_drawPinLabels(using: context)
		_drawLabel(using: context)
	}

	private func _drawFrame(using context : ShapeRenderContext)
	{
		let outerNutWidth = self.size.width * 0.32
		let innerNutWidth = self.size.width * 0.16
		let nutInset = (context.flipped ? -1 : 1) * innerNutWidth * 0.3
		let topY = (context.flipped ? -1 : 1) * self.size.height/2.0
		let bottomY = -topY
		let cgContext = context.graphicsContext
		cgContext.beginPath()
		let posLeftSide = skDIPLeadLength - self.size.width/2.0
		cgContext.move(to: CGPoint(x: posLeftSide, y:bottomY))
		cgContext.addLine(to: CGPoint(x: posLeftSide, y:topY))
	#if false // curved vs. angled notch
		let radius = self.size.width * 0.075
		cgContext.addLine(to: CGPoint(x:-radius, y:topY))
		cgContext.addArc(center: CGPoint(x:0,y:bottomY), radius: radius, startAngle: .pi, endAngle: 0, clockwise: false)
	#else
		cgContext.addLine(to: CGPoint(x: -outerNutWidth/2.0, y: topY))
		cgContext.addLine(to: CGPoint(x: -innerNutWidth/2.0, y: topY - nutInset))
		cgContext.addLine(to: CGPoint(x: innerNutWidth/2.0, y: topY - nutInset))
		cgContext.addLine(to: CGPoint(x: outerNutWidth/2.0, y: topY))
	#endif
		let posRightSide = self.size.width/2.0 - skDIPLeadLength
		cgContext.addLine(to: CGPoint(x: posRightSide, y: topY))
		cgContext.addLine(to: CGPoint(x: posRightSide, y: bottomY))
		cgContext.closePath()
	}

	private func _drawLeads(using context : ShapeRenderContext)
	{
		for i in 0..<_leadCount
		{
			let leadEndLocation = _locationOfPin(at: i, flippedVertically: context.flipped)
			let onLeftSide = i < _leadCount/2
			let leadStrokeOffset : CGFloat = onLeftSide ? skDIPLeadLength : -skDIPLeadLength
			context.graphicsContext.move(to: leadEndLocation)
			context.graphicsContext.addLine(to: CGPoint(x: leadEndLocation.x + leadStrokeOffset, y: leadEndLocation.x))
		}
		context.graphicsContext.strokePath()
	}

	private func _drawPinLabels(using context : ShapeRenderContext)
	{
		var textAlignment = DIPShapeAlignment()
		for i in 0..<_leadCount
		{
			let pinLocation = _locationOfPin(at: i, flippedVertically: context.flipped)
			let onLeftSide = i < _leadCount/2
			textAlignment.horizontal = onLeftSide ? .left : .right
			let leadLabelOffsetX = skDIPLeadLength + skDIPLabelInset
			let leadLabelPosition = CGPoint(x: onLeftSide ? pinLocation.x + leadLabelOffsetX : pinLocation.x - leadLabelOffsetX, y: pinLocation.y)
			context.graphicsContext.saveGState()
			_drawPinLabel("\(i+1)", location: leadLabelPosition, alignment: textAlignment, using: context)
			context.graphicsContext.restoreGState()
		}
	}

	private func _drawLabel(using context : ShapeRenderContext)
	{
		guard let label = self.attributes["label"] as String? else { return }
		let centerAlignment = DIPShapeAlignment()
		context.graphicsContext.saveGState()
		defer { context.graphicsContext.restoreGState() }
		context.graphicsContext.rotate(by: .pi/2)
		_drawDIPLabel(label, location: .zero, alignment: centerAlignment, using: context)
	}

	private func _calculateSize() -> CGSize
	{
		CGSize(width: skDIPWidth, height: (CGFloat(_leadCount/2 - 1) * skDIPLeadSpacing) + (2 * skDIPMargin))
	}

	private func _createLeadPoints() -> [ConnectionPoint]
	{
  	var leadPoints = [ConnectionPoint]()
  	for leadIndex in 0..<_leadCount
		{
			var connectionPoint = ConnectionPoint()
			connectionPoint.name = "\(leadIndex+1)"
			connectionPoint.location = _locationOfPin(at: leadIndex, flippedVertically: false)
			leadPoints.append(connectionPoint)
		}
		return leadPoints
	}

	private func _locationOfPin(at pinIndex : UInt, flippedVertically : Bool) -> CGPoint
	{
		let isLeftSide = pinIndex < _leadCount / 2
		let numLeadsFromTop = isLeftSide ? pinIndex : (_leadCount - 1 - pinIndex)
		return CGPoint(
			x: ( isLeftSide ? -self.size.width : self.size.width) / 2.0,
			y: (flippedVertically ? -1 : 1) * (self.size.height/2.0 - skDIPMargin - (CGFloat(numLeadsFromTop) * skDIPLeadSpacing))
		)
	}

	private func _drawPinLabel(
		_ text : String,
		location : CGPoint,
		alignment : DIPShapeAlignment,
		using context : ShapeRenderContext)
	{
		let textAttributes : [NSAttributedString.Key : Any] = [
			.font : NSFont(name: skDIPTextFontName, size: 10)!,
			.foregroundColor : NSColor(white: 0.8, alpha: 1.0)
		]
		_drawText(text, location: location, alignment: alignment, attributes: textAttributes, using: context)
	}

	private func _drawDIPLabel(
		_ text : String,
		location : CGPoint,
		alignment : DIPShapeAlignment,
		using context : ShapeRenderContext)
	{
		let textAttributes : [NSAttributedString.Key : Any] = [
			.font : NSFont(name: skDIPTextFontName, size: 10)!,
			.foregroundColor : context.textColor != nil ? NSColor(cgColor:context.textColor!)! : NSColor.black
		]
		_drawText(text, location: location, alignment: alignment, attributes: textAttributes, using: context)
	}

	private func _drawText(
		_ text : String,
		location : CGPoint,
		alignment : DIPShapeAlignment,
		attributes : [NSAttributedString.Key:Any],
		using context : ShapeRenderContext)
	{
		let attributedString = NSAttributedString(string: text, attributes: attributes) as CFAttributedString
		let textLine = CTLineCreateWithAttributedString(attributedString)
		let textSize = CTLineGetImageBounds(textLine, context.graphicsContext)

		var textLocation = location
		switch alignment.horizontal
		{
			case .center: textLocation.x -= textSize.width/2.0
			case .right: textLocation.x -= textSize.width
			case .left: break
		}
		switch alignment.vertical
		{
			case .center: textLocation.y += (context.flipped ? 1 : -1) * textSize.height/2.0
			case .top: textLocation.y += (context.flipped ? 1 : -1) * textSize.height
			case .bottom: break
		}
		context.graphicsContext.textMatrix = CGAffineTransform(translationX: textLocation.x, y: textLocation.y)
		CTLineDraw(textLine, context.graphicsContext)
	}
}
