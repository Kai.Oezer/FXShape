/**
This file is part of the FXShape project.
Copyright (C) 2021 Kai Berk Oezer
https://gitlab.com/Kai.Oezer/FXShape

FXShape is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import AppKit

public protocol ShapeViewDelegate : AnyObject
{
	var draggingContentWriter : NSPasteboardWriting { get }
}

public class ShapeView : NSView
{
	public enum ScaleMode
	{
		case none
		case fitToView      // up or down scaling
		case scaleDownToFit // no scaling up
		case scaleUpToFit   // no scaling down
	}

	public enum VerticalAlignment
	{
		case top
		case center
		case bottom
	}

	public var shapeAttributes = [String:String]()
	public var delegate : ShapeViewDelegate?
	public var isCached : Bool = false
	public var isBordered : Bool = false
	public var isDraggable : Bool = true
	public var scaleMode : ScaleMode = .none
	public var verticalAlignment : VerticalAlignment = .center
	public var rotation : CGFloat = 0.0
	public var shapeColor = CGColor.black
	public var selectedShapeColor = CGColor.white

	private var _imageCacheNeedsRefreshing = true
	private let _disabledShapeColor = CGColor(gray: 0.5, alpha: 1.0)
	private var _draggingStartedInsideTheView = false
	private var _lastMouseDownLocation : NSPoint = .zero
	private var _dragIcon : NSImage?
	private let _colorSpace = CGColorSpace(name: CGColorSpace.sRGB)

	private let skLineWidthCompensation = CGFloat(2.0)

  // TODO: In order to support on-the-fly switching between Hi-DPI and normal DPI,
  // the image cache should be an NSImage containing two NSBitmapImageRep instances.
  var _imageCache : CGImage?

	public var shape : Shape?
	{
		didSet
		{
			_dragIcon = nil
			_imageCacheNeedsRefreshing = true
			self.needsDisplay = true
		}
	}

	public var enabled : Bool = true
	{
		didSet
		{
			_imageCacheNeedsRefreshing = true
			self.needsDisplay = true
		}
	}

	public var isSelected : Bool = false
	{
		didSet
		{
			_imageCacheNeedsRefreshing = true
			self.needsDisplay = true
		}
	}

	var shapeSize : NSSize { self.shape?.size ?? .zero }


	var draggingImage : NSImage?
	{
		if _dragIcon == nil
		{
			if _imageCache == nil
			{
				_refreshImageCache()
			}
			if let shape = self.shape, let image = _imageCache
			{
				_dragIcon = ShapeRenderer.shared.image(from: image, pointSize: shape.size)
			}
		}
		return _dragIcon
	}

	public override func setFrameSize(_ newSize: NSSize)
	{
		super.setFrameSize(newSize)
		if self.isCached
		{
			_refreshImageCache()
		}
	}

	public override func acceptsFirstMouse(for event: NSEvent?) -> Bool
	{
		true
	}

	public override func draw(_ rect : NSRect)
	{
		guard var shape = self.shape, let context = NSGraphicsContext.current?.cgContext else { return }
		if self.isCached
		{
			if _imageCache == nil || _imageCacheNeedsRefreshing
			{
				_refreshImageCache()
			}
			assert(_imageCache != nil)
		}

		let scaleFactor = _calculateScaleFactor()
		let frameSize = self.bounds.size
		let halfWidth = frameSize.width / 2.0
		let halfHeight = frameSize.height / 2.0

		context.textMatrix = .identity

		context.saveGState()
		if self.verticalAlignment != .center
		{
			let shapeSize = shape.size
			let verticalOffset = (frameSize.height - (scaleFactor * shapeSize.height))/2.0 - skLineWidthCompensation
			context.translateBy(x: 0, y: (self.verticalAlignment == .top) ? verticalOffset : -verticalOffset)
		}
		if self.rotation != 0.0
		{
			context.translateBy(x: halfWidth, y: halfHeight)
			context.rotate(by: self.rotation)
			context.translateBy(x: -halfWidth, y: -halfHeight)
		}
		if self.isCached
		{
			_drawImage(in: context, scaleFactor: scaleFactor)
		}
		else
		{
			if shape.doesOwnDrawing && shape.attributes.isEmpty && !self.shapeAttributes.isEmpty
			{
				shape.attributes = self.shapeAttributes
			}
			_drawShape(in: context, scaleFactor: scaleFactor)
		}
		context.restoreGState()

		if self.isBordered
		{
			context.setStrokeColor(CGColor.black)
			context.stroke(rect)
		}
	}

	public override func mouseDown(with event: NSEvent)
	{
		guard self.isDraggable else { super.mouseDown(with: event); return }
		_lastMouseDownLocation = self.convert(event.locationInWindow, from: nil)
		_draggingStartedInsideTheView = true
	}

	public override func mouseDragged(with event: NSEvent)
	{
		guard self.isDraggable && _draggingStartedInsideTheView else { super.mouseDragged(with: event); return }
		let currentLocation = self.convert(event.locationInWindow, from: nil)
		let distance = hypot( currentLocation.x - _lastMouseDownLocation.x, currentLocation.y - _lastMouseDownLocation.y );
		if distance > 3.0 // tolerance against jittery clicks vs. dragging
		{
			if let draggedObjects = _draggingItems(for: currentLocation)
			{
				let draggingSession = self.beginDraggingSession(with: draggedObjects, event: event, source: self)
				draggingSession.animatesToStartingPositionsOnCancelOrFail = true
				_draggingStartedInsideTheView = false
			}
		}
	}

	public override func mouseUp(with event: NSEvent)
	{
		_draggingStartedInsideTheView = false
	}

	public override func shouldDelayWindowOrdering(for event: NSEvent) -> Bool
	{
		true // So that a shape can be dragged without activating the window that contains the shape view.
	}

	private func _refreshImageCache()
	{
		guard let shape = self.shape else { return }
		let shapeColor = self.enabled ? (self.isSelected ? self.selectedShapeColor : self.shapeColor) : _disabledShapeColor
		let hiDPI = (self.window?.backingScaleFactor ?? 1.0) > 1.5
		_imageCache = ShapeRenderer.shared.newImage(from:shape, backgroundColor:nil, strokeColor:shapeColor, fillColor:shapeColor, scaleFactor:1.0, forHiDPI:hiDPI)
		_imageCacheNeedsRefreshing = false
	}

	private func _calculateScaleFactor() -> CGFloat
	{
		guard let shape = self.shape else { assertionFailure("invalid shape"); return 1.0 }
		let frameSize = self.bounds.size
		let availableSize = CGSize(width: frameSize.width - skLineWidthCompensation, height: frameSize.height - skLineWidthCompensation)
		let shapeSize = shape.size
		let resizeTheWidth = ((self.scaleMode == .fitToView) && (shapeSize.width != availableSize.width))
			|| ((self.scaleMode == .scaleUpToFit) && (shapeSize.width < availableSize.width))
			|| ((self.scaleMode == .scaleDownToFit) && (shapeSize.width > availableSize.width))
		let resizeTheHeight = ((self.scaleMode == .fitToView) && (shapeSize.height != availableSize.height))
			|| ((self.scaleMode == .scaleUpToFit) && (shapeSize.height < availableSize.height))
			|| ((self.scaleMode == .scaleDownToFit) && (shapeSize.height > availableSize.height))
		let scaleFactorX = resizeTheWidth ? (availableSize.width/shapeSize.width) : 1.0
		let scaleFactorY = resizeTheHeight ? (availableSize.height/shapeSize.height) : 1.0
		return min(scaleFactorX, scaleFactorY)
	}

	private func _drawImage(in context : CGContext, scaleFactor : CGFloat)
	{
		guard let shapeSize = self.shape?.size, let image = _imageCache else { return }
	  let frameSize = self.bounds.size
		let imageRect = CGRect(
			x: floor((frameSize.width - floor(scaleFactor * shapeSize.width))/2),
			y: floor((frameSize.height - floor(scaleFactor * shapeSize.height))/2),
			width: floor(scaleFactor * shapeSize.width),
			height: floor(scaleFactor * shapeSize.height))
		context.draw(image, in: imageRect)
	}

	private func _drawShape(in context : CGContext, scaleFactor : CGFloat)
	{
		guard let shape = self.shape else { return }
		let frameSize = self.bounds.size
		let shapeColor = (self.enabled ? (self.isSelected ? self.selectedShapeColor : self.shapeColor) : _disabledShapeColor)
		context.translateBy(x: frameSize.width/2, y: frameSize.height/2)
		let renderContext = ShapeRenderContext(graphicsContext: context, strokeColor: shapeColor, textColor: shapeColor, flipped: false)
		ShapeRenderer.shared.renderShape(shape, using: renderContext, scaleFactor: scaleFactor, forHiDPI:false)
	}

	private func _draggingItems(for draggingLocation : NSPoint) -> [NSDraggingItem]?
	{
		guard let delegate = self.delegate, let draggingImage = self.draggingImage else { return nil }
		let draggingWriter = delegate.draggingContentWriter
		let viewFrameSize = self.frame.size
		let draggingItem = NSDraggingItem(pasteboardWriter: draggingWriter)
		draggingItem.draggingFrame = NSRect(origin: .zero, size: viewFrameSize)
		draggingItem.imageComponentsProvider = {
			let imageComponents = NSDraggingImageComponent(key:.icon)
			let iconSize = draggingImage.size
			let offsetX = round((viewFrameSize.width - iconSize.width)/2)
			let offsetY = round((viewFrameSize.height - iconSize.height)/2)
			imageComponents.frame = NSRect(x: offsetX, y: offsetY, width: round(iconSize.width), height: round(iconSize.height))
			imageComponents.contents = draggingImage
			return [imageComponents]
		}
		return [draggingItem]
	}
}

extension ShapeView : NSDraggingSource
{
	public func draggingSession(_ session: NSDraggingSession, sourceOperationMaskFor context: NSDraggingContext) -> NSDragOperation
	{
		.generic
	}
}
