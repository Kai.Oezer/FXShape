/**
This file is part of the FXShape project.
Copyright (C) 2021 Kai Berk Oezer
https://gitlab.com/Kai.Oezer/FXShape

FXShape is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import Foundation

public class ShapeFactory
{
	static let FXVolta_SubcircuitShapeType = "FXVolta_SubcircuitShapeType"
	static let FXVolta_SubcircuitShapeLabel = "FXVolta_SubcircuitShapeLabel"

	let shared = ShapeFactory()

	static let skDIPPrefix = "DIP"

	private init() {}

	public static func makeShape(paths : [Path], circles : [Circle], connectionPoints : [ConnectionPoint], size : CGSize) -> Shape
	{
		BasicShape(paths: paths, circles: circles, connectionPoints: connectionPoints, size: size)
	}

	public static func makeShape(from text : String) -> Shape
	{
		let shape = TextShape()
		shape.attributes = [ "text" : text ]
		return shape
	}

	public static func makeShape(from metadata : [String : String]) -> Shape?
	{
		var shape : Shape?
		var label = ""
		for item in metadata
		{
			if shape == nil && item.key == FXVolta_SubcircuitShapeType
			{
				let shapeTypeName = item.value
				if shapeTypeName.starts(with: skDIPPrefix)
				{
					if let pinCount = UInt(shapeTypeName[shapeTypeName.index(shapeTypeName.startIndex, offsetBy: skDIPPrefix.count)...])
					{
						shape = makeDIPShape(pinCount: pinCount)
					}
				}
			}
			else if label.isEmpty && item.key == FXVolta_SubcircuitShapeLabel
			{
				label = item.value
			}
			if shape != nil && !label.isEmpty
			{
				break
			}
		}
		guard !label.isEmpty, var shape = shape else { return nil }
		shape.attributes = ["label" : label]
		return shape
	}

	public static func makeDIPShape(pinCount : UInt) -> Shape
	{
		DIPShape(leadCount: pinCount)
	}
}
