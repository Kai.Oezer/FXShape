/**
This file is part of the FXShape project.
Copyright (C) 2021 Kai Berk Oezer
https://gitlab.com/Kai.Oezer/FXShape

FXShape is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import Foundation
import Collections

fileprivate let PathClosureSuffix = "z"

public struct Path : Codable
{
	public var segments = [Segment]()
	public var filled : Bool = false
	public var closed : Bool = false

	public var boundingBox : CGRect
	{
		.zero
	}

	public init()
	{
	}

	/// - parameter data: SVG path data
	public init(data : String)
	{
		var trimmedData = data.replacingOccurrences(of: ",", with: " ").trimmingCharacters(in: .whitespaces)
		self.closed = trimmedData.hasSuffix(PathClosureSuffix)
		if self.closed
		{
			let sub = trimmedData[trimmedData.startIndex..<trimmedData.index(trimmedData.endIndex, offsetBy: -PathClosureSuffix.count)]
			trimmedData = String(sub.trimmingCharacters(in: .whitespaces))
		}
		self.segments = extractSegments(from: trimmedData)
	}
}

extension Path : Identifiable
{
	public var id : String
	{
		["P", filled ? "f" : "", closed ? "z" : "", segments.map{ $0.id }.joined(separator: "|")].joined(separator:"")
	}
}

////////////////////////////////////////////////////////////////////////////////
// SVG 1.1 PATH DATA FORMAT
// See implementation notes at http://www.w3.org/TR/SVG/implnote.html
////////////////////////////////////////////////////////////////////////////////
// All instructions are expressed as one character (e.g., a moveto is expressed
// as an M). Superfluous white space and separators such as commas can be
// eliminated (e.g., "M 100 100 L 200 200" contains unnecessary spaces and could
// be expressed more compactly as "M100 100L200 200").
// The command letter can be eliminated on subsequent commands if the same
// command is used multiple times in a row (e.g., you can drop the second "L" in
// "M 100 200 L 200 100 L -100 -200" and use "M 100 200 L 200 100 -100 -200"
// instead).
// Relative versions of all commands are available (uppercase means absolute
// coordinates, lowercase means relative coordinates).
// Alternate forms of lineto are available to optimize the special cases of
// horizontal and vertical lines (absolute and relative).
// Alternate forms of curve are available to optimize the special cases where
// some of the control points on the current segment can be determined
// automatically from the control points on the previous segment.
////////////////////////////////////////////////////////////////////////////////

fileprivate func extractFloats(from argumentStack : Deque<String>) -> [CGFloat]
{
	argumentStack.compactMap{ Float($0) }.map{ CGFloat($0) }
}

fileprivate func extractSegments(from pathData : String) -> [Segment]
{
	var result = Deque<Segment>()
	let tokens = pathData.components(separatedBy: .whitespaces)
	guard !tokens.isEmpty else { return [] }
	var argumentsDeque = Deque<String>()
	for (index, token) in tokens.reversed().enumerated()
	{
		let isFirstCommand = (index == (tokens.count - 1))
		if let startSegments = startSegments(from: token, arguments: argumentsDeque, isFirstCommand: isFirstCommand), !startSegments.isEmpty
		{
			result.prepend(contentsOf:startSegments)
			argumentsDeque.removeAll()
		}
		else if let segments = segments(from: token, arguments: argumentsDeque), !segments.isEmpty
		{
			result.prepend(contentsOf: segments)
			argumentsDeque.removeAll()
		}
		else
		{
			// the token is not a command, it must be a command argument (= floating point number)
			if Float(token) != nil
			{
				argumentsDeque.prepend(token)
			}
		}
	}
	return Array<Segment>(result)
}

/// According to the SVG specification, if a relative `moveto` (`m`) appears as
/// the first element of the path, then it is treated as a pair of absolute coordinates.
/// Furthermore, in this case, subsequent pairs of coordinates are treated as
/// relative even though the initial `moveto` is interpreted as an absolute `moveto`.
/// If a `moveto` is followed by multiple pairs of coordinates, the subsequent pairs
/// are treated as implicit lineto commands.
/// http://www.w3.org/TR/SVG/paths.html#PathDataMovetoCommands
fileprivate func startSegments(from token : String, arguments : Deque<String>, isFirstCommand : Bool) -> [Segment]?
{
	guard token == "M" || token == "m" else { return nil }
	let isRelativeMove = !isFirstCommand && (token == "m")
	let isRelativeLine = isFirstCommand || (token == "m")
	let f = extractFloats(from: arguments)
	guard (f.count >= 2) && (f.count % 2 == 0) else { return nil }
	var segments = [Segment]()
	segments.append(.jump(JumpSegment(destination: Vector2D(f[0], f[1]), relative: isRelativeMove)))
	for index in stride(from: 2, to: f.count, by: 2)
	{
		segments.append(.line(LineSegment(destination: Vector2D(f[index], f[index+1]), relative: isRelativeLine)))
	}
	return segments
}

fileprivate func segments(from token : String, arguments : Deque<String>) -> [Segment]?
{
	arcSegments(from: token, arguments: arguments)
	?? ( lineSegments(from: token, arguments: arguments)
	?? ( verticalLineSegments(from: token, arguments: arguments)
	?? ( horizontalLineSegments(from: token, arguments: arguments)
	?? curveSegments(from: token, arguments: arguments) )))
}

fileprivate func arcSegments(from token : String, arguments : Deque<String>) -> [Segment]?
{
	guard token == "A" || token == "a" else { return nil }
	let f = extractFloats(from: arguments)
	guard (f.count >= 7) && (f.count % 7 == 0) else { return nil }
	var result = [Segment]()
	for index in stride(from: 0, to: f.count, by: 7)
	{
		result.append(
			.arc(ArcSegment(
				radiusX: f[index],
				radiusY: f[index+1],
				rotation: f[index+2],
				largeArc: f[index+3] > 0.5,
				positiveSweep: f[index+4] > 0.5,
				endPoint: Vector2D(f[index+5], f[index+6]),
				relative: token == "a"
			))
		)
	}
	return result
}

fileprivate func lineSegments(from token : String, arguments : Deque<String>) -> [Segment]?
{
	guard token == "L" || token == "l" else { return nil }
	let f = extractFloats(from: arguments)
	guard (f.count >= 2) && (f.count % 2 == 0) else { return nil }
	var result = [Segment]()
	for index in stride(from: 0, to: f.count, by: 2)
	{
		result.append(.line(LineSegment(destination: Vector2D(f[index], f[index+1]), relative: (token == "l"))))
	}
	return result
}

fileprivate func verticalLineSegments(from token : String, arguments : Deque<String>) -> [Segment]?
{
	guard token == "V" || token == "v" else { return nil }
	// the SVG standard allows multiple arguments to 'V'/'v'
	let f = extractFloats(from: arguments)
	guard !f.isEmpty else { return nil }
	let totalDistance = f.reduce(0.0){ $0 + $1 }
	return [.verticalLine(VerticalLineSegment(distance: totalDistance, relative: (token == "v")))]
}

fileprivate func horizontalLineSegments(from token : String, arguments : Deque<String>) -> [Segment]?
{
	guard token == "H" || token == "h" else { return nil }
	// the SVG standard allows multiple arguments to 'H'/'h'
	let f = extractFloats(from: arguments)
	guard !f.isEmpty else { return nil }
	let totalDistance = f.reduce(0.0){ $0 + $1 }
	return [.horizontalLine(HorizontalLineSegment(distance: totalDistance, relative: (token == "h")))]
}

fileprivate func curveSegments(from token : String, arguments : Deque<String>) -> [Segment]?
{
	guard token == "C" || token == "c" else { return nil }
	let f = extractFloats(from: arguments)
	guard (f.count >= 6) && (f.count % 6 == 0) else { return nil }
	var result = [Segment]()
	for index in stride(from: 0, to: f.count, by: 6)
	{
		result.append(.curve(CurveSegment(
			controlPoint1: Vector2D(f[index + 0], f[index + 1]),
			controlPoint2: Vector2D(f[index + 2], f[index + 3]),
			endPoint: Vector2D(f[index + 4], f[index + 5]),
			relative: (token == "c"))))
	}
	return result
}
