/**
This file is part of the FXShape project.
Copyright (C) 2021 Kai Berk Oezer
https://gitlab.com/Kai.Oezer/FXShape

FXShape is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import AppKit
import CoreGraphics

fileprivate let defaultStringAttributes : [NSAttributedString.Key : Any] = [
	.font : NSFont(name: "Lucida Grande", size: 18) ?? NSFont.systemFont(ofSize: 18),
	.foregroundColor : NSColor.black
]

enum TextShapeAttribute : String, Codable
{
	case text = "text"
	case color = "transient-text-color"
	case font = "text-font"
}

class TextShape : Shape
{
	public var paths : [Path] { [] }
	public var circles : [Circle] { [] }
	public var connectionPoints : [ConnectionPoint] { [] }
	public private(set) var size : CGSize = .zero
	public var doesOwnDrawing: Bool { true }
	public var isReusable: Bool { false }

	public var id : String { "Text_\(attributes.map{"\($0.key):\($0.value)"}.joined(separator: "_"))" }

	public var attributes = [String:String]()
	{
		didSet { _updateSize() }
	}

	public func draw(context: ShapeRenderContext)
	{
		context.graphicsContext.saveGState()
		defer { context.graphicsContext.restoreGState() }
		if !context.flipped
		{
			context.graphicsContext.textMatrix = CGAffineTransform(scaleX:1, y:-1)
		}
		_drawText(with: context.graphicsContext)
	}

	private var _effectiveStringAttributes : [NSAttributedString.Key : Any]
	{
		var stringAttributes = defaultStringAttributes
		if let color = self.attributes[TextShapeAttribute.color.rawValue]
		{
			stringAttributes[.foregroundColor] = color
		}
		if let font = self.attributes[TextShapeAttribute.font.rawValue]
		{
			stringAttributes[.font] = font
		}
		return stringAttributes
	}

	private func _drawText(with context : CGContext)
	{
		guard let text = self.attributes[TextShapeAttribute.text.rawValue] else { return }
		let stringAttributes = _effectiveStringAttributes
		let attributedString = NSAttributedString(string: text, attributes: stringAttributes)
		let textLine = CTLineCreateWithAttributedString(attributedString)
		let textSize = CTLineGetImageBounds(textLine, context).size
		context.textMatrix = CGAffineTransform(translationX: floor(-textSize.width/2), y: floor(-textSize.height/2))
		CTLineDraw(textLine, context)
	}

	private func _updateSize()
	{
		self.size = .zero
		guard let text = self.attributes[TextShapeAttribute.text.rawValue] else { return }
		let stringAttributes = _effectiveStringAttributes
		self.size = text.size(withAttributes: stringAttributes)
		self.size.width = ceil(self.size.width)
		self.size.height = ceil(self.size.height)
	}
}
