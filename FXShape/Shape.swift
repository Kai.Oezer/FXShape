/**
This file is part of the FXShape project.
Copyright (C) 2021 Kai Berk Oezer
https://gitlab.com/Kai.Oezer/FXShape

FXShape is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import Foundation
import CoreGraphics

public struct ShapeRenderContext
{
	let graphicsContext : CGContext
	let strokeColor : CGColor?
	let textColor : CGColor?
	let flipped : Bool // whether Y axis values are increasing from top to bottom
}

public protocol Shape : Codable
{
	var id : String { get }
	var paths : [Path] { get }
	var circles : [Circle] { get }
	var connectionPoints : [ConnectionPoint] { get }
	var size : CGSize { get }
	var doesOwnDrawing : Bool { get }
	var isReusable : Bool { get }
	var attributes : [String:String] { get set }

	/// Called only if `doesOwnDrawing == true`
	func draw(context : ShapeRenderContext)
}

public struct AnyShape : Hashable, Identifiable
{
	var shape : Shape

	public init(_ shape : Shape) { self.shape = shape }

	public var id : String { shape.id }
	public static func == (lhs: Self, rhs: Self) -> Bool { lhs.id == rhs.id }
	public var hashValue: Int { id.hashValue }
	public func hash(into hasher: inout Hasher) { hasher.combine(id) }
}


/*
public class IdentifiableShape : Shape, Identifiable
{
	public let id = UUID()

	public init<H : Shape>(_ base : H) { self.base = base }

	var base : AnyObject & Shape

	public var paths : [Path] { base.paths }
	public var circles : [Circle] { base.circles }
	public var connectionPoints : [ConnectionPoint] { base.connectionPoints }
	public var size : CGSize { base.size }
	public var doesOwnDrawing : Bool { base.doesOwnDrawing }
	public var isReusable : Bool { base.isReusable }
	public var attributes : [String:String]
	{
		get { base.attributes }
		set { base.attributes = newValue }
	}
	public func draw(context: ShapeRenderContext)
	{
		base.draw(context: context)
	}
}

extension IdentifiableShape : Hashable
{
	public var hashValue: Int { id.hashValue }

	public func hash(into hasher: inout Hasher)
	{
		hasher.combine(id)
	}

	public static func == (lhs: IdentifiableShape, rhs: IdentifiableShape) -> Bool
	{
		lhs.id == rhs.id
	}
}
*/
