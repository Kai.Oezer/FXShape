/**
This file is part of the FXShape project.
Copyright (C) 2021-2024 Kai Berk Oezer
https://gitlab.com/Kai.Oezer/FXShape

FXShape is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import Foundation

public enum Segment : Codable
{
	case jump(JumpSegment)
	case line(LineSegment)
	case verticalLine(VerticalLineSegment)
	case horizontalLine(HorizontalLineSegment)
	case arc(ArcSegment)
	case curve(CurveSegment)
}

extension Segment : Identifiable
{
	public var id : String
	{
		switch self
		{
			case .jump(let s) : return s.id
			case .line(let s) : return s.id
			case .verticalLine(let s) : return s.id
			case .horizontalLine(let s) : return s.id
			case .arc(let s) : return s.id
			case .curve(let s) : return s.id
		}
	}
}

// MARK: -

/// Segment which jumps to the given point without drawing anything.
///
/// Corresponds to SVG's "M" (or "m") path data command.
public struct JumpSegment : Codable, Identifiable
{
	public let destination : Vector2D
	public private(set) var isRelative : Bool

	public var id : String { "J" + (isRelative ? "r" : "") + "_\(destination)" }

	public init(destination : Vector2D, relative : Bool)
	{
		self.destination = destination
		self.isRelative = relative
	}
}

// MARK: -

/// Segment which draws a line from the current point to a given point.
public struct LineSegment : Codable, Identifiable
{
	public let destination : Vector2D
	public private(set) var isRelative : Bool

	public var id : String { "L" + (isRelative ? "r" : "") + "_\(destination)" }

	public init(destination : Vector2D, relative : Bool)
	{
		self.destination = destination
		self.isRelative = relative
	}
}

// MARK: -

public struct VerticalLineSegment : Codable, Identifiable
{
	public let distance : CGFloat
	public private(set) var isRelative : Bool

	public var id : String { "V" + (isRelative ? "r" : "") + "_\(distance)" }

	public init(distance : CGFloat, relative : Bool)
	{
		self.distance = distance
		self.isRelative = relative
	}
}

// MARK: -

public struct HorizontalLineSegment : Codable, Identifiable
{
	public let distance : CGFloat
	public private(set) var isRelative : Bool

	public var id : String { "H" + (isRelative ? "r" : "") + "_\(distance)" }

	public init(distance : CGFloat, relative : Bool)
	{
		self.distance = distance
		self.isRelative = relative
	}
}

// MARK: -

/// An elliptical arc segment in SVG style.
///
/// The end point of the arc is one of the parameters. This is unusual for
/// 2D graphics libraries and necessitates conversion before the drawing
/// routines are called.
public struct ArcSegment : Codable, Identifiable
{
	public var endPoint : Vector2D
	public var radiusX : CGFloat
	public var radiusY : CGFloat
	public var rotation : CGFloat
	public var largeArc : Bool
	public var sweepPositive : Bool
	public private(set) var isRelative : Bool

	public var id : String { "A" + (isRelative ? "r" : "") + "_\(endPoint)_\(radiusX)_\(radiusY)_\(rotation)" }

	/// Draws elliptical arc from current position to given end position
	///
	///	- parameters:
	///   - radiusX: maximum extent of the ellipsis (from the center point) along the x axis
	///   - radiusY: maximum extent of the ellipsis (from the center point) along the y axis
	///   - rotation: angle (in radian) by which the arc should be rotated around the current point
	///   - largeArc: indicates whether to draw the larger one of the two possible arc sizes
	///   - positiveSweep: indicates whether the arc sweeps in the counterclockwise direction
	///   - relative: whether the end position is relative or absolute to the current position
	public init(
		radiusX : CGFloat,
		radiusY : CGFloat,
		rotation : CGFloat,
		largeArc : Bool,
		positiveSweep : Bool,
		endPoint : Vector2D,
		relative : Bool
	)
	{
		assert(radiusX > 0.0, "radius must be positive")
		assert(radiusY > 0.0, "radius must be positive")
		self.radiusX = radiusX
		self.radiusY = radiusY
		self.rotation = rotation
		self.largeArc = largeArc
		self.sweepPositive = positiveSweep
		self.endPoint = endPoint
		self.isRelative = relative
	}
}

// MARK: -

/// Cubic Bezier curve.
///
/// Corresponds to SVG's "C" (or "c") path data commands.
public struct CurveSegment : Codable, Identifiable
{
	public var controlPoint1 : Vector2D
	public var controlPoint2 : Vector2D
	public var endPoint : Vector2D
	public private(set) var isRelative : Bool

	public var id : String { "C" + (isRelative ? "r" : "") + "_\(controlPoint1)_\(controlPoint2)_\(endPoint)" }

	init(
		controlPoint1 : Vector2D,
		controlPoint2 : Vector2D,
		endPoint : Vector2D,
		relative : Bool
	)
	{
		self.controlPoint1 = controlPoint1
		self.controlPoint2 = controlPoint2
		self.endPoint = endPoint
		self.isRelative = relative
	}
}
