/**
This file is part of the FXShape project.
Copyright (C) 2021 Kai Berk Oezer
https://gitlab.com/Kai.Oezer/FXShape

FXShape is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import AppKit

public class ShapeRenderer
{
	public static let shared = ShapeRenderer()

	private let _renderDataPool = ShapeRenderDataPool()

	private init() {}

	public func renderShape(
		_ shape : Shape,
		using context : ShapeRenderContext,
		scaleFactor : CGFloat,
		forHiDPI : Bool = true)
	{
		let scale = forHiDPI ? 2 * scaleFactor : scaleFactor

		let cgContext = context.graphicsContext
		cgContext.saveGState()
		defer { cgContext.restoreGState() }

		if shape.doesOwnDrawing
		{
			cgContext.scaleBy(x: scale, y: scale)
			shape.draw(context: context)
			return
		}
		guard let renderData = _renderDataPool[AnyShape(shape)] else { return }
		if let strokeColor = context.strokeColor
		{
			cgContext.setStrokeColor(strokeColor)
			cgContext.setFillColor(strokeColor)
		}
		cgContext.scaleBy(x: scale, y: context.flipped ? -scale : scale)

		for pathInfo in renderData
		{
			cgContext.beginPath()
			cgContext.addPath(pathInfo.path)
			if pathInfo.filled
			{
				cgContext.fillPath()
			}
			else
			{
				cgContext.strokePath()
			}
		}
	}

	public func newImage(
		from shape : Shape,
		backgroundColor : CGColor?,
		strokeColor : CGColor,
		fillColor : CGColor,
		scaleFactor : CGFloat,
		forHiDPI : Bool = true) -> CGImage?
	{
		// The effective width and height are a little larger due to stroke width
		let lineWidthCompensation = CGFloat(2)
		let shapeWidth = Int(ceil(scaleFactor*(ceil(shape.size.width) + lineWidthCompensation)))
		let shapeHeight = Int(ceil(scaleFactor*(ceil(shape.size.height) + lineWidthCompensation)))
		let bitmapWidth = forHiDPI ? 2 * shapeWidth : shapeWidth
		let bitmapHeight = forHiDPI ? 2 * shapeHeight : shapeHeight
		let numComponents = 4 // RGBA, 8 bit each
		let bytesPerComponent = 1
		let bytesPerRow = bitmapWidth * numComponents * bytesPerComponent
		let bitmapData = UnsafeMutableRawPointer.allocate(byteCount: bitmapHeight * bytesPerRow, alignment: 4)
		defer { free(bitmapData) }
		guard let bitmapContext = CGContext(data: bitmapData, width: bitmapWidth, height: bitmapHeight, bitsPerComponent: 8 * bytesPerComponent, bytesPerRow: bytesPerRow, space: CGColorSpaceCreateDeviceRGB(), bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)
			else { assertionFailure("Image cache drawing context could not be created."); return nil }

		if let backgroundColor = backgroundColor
		{
			bitmapContext.setFillColor(backgroundColor)
			bitmapContext.fill(CGRect(origin:.zero, size: CGSize(width:bitmapWidth, height:bitmapHeight)))
		}
		bitmapContext.setStrokeColor(strokeColor)
		bitmapContext.setFillColor(fillColor)
		bitmapContext.translateBy(x: round(CGFloat(bitmapWidth/2)), y: round(CGFloat(bitmapHeight/2)))
		let renderContext = ShapeRenderContext(graphicsContext: bitmapContext, strokeColor: strokeColor, textColor: strokeColor, flipped: false)
		renderShape(shape, using: renderContext, scaleFactor: scaleFactor, forHiDPI: forHiDPI)
		return bitmapContext.makeImage()
	}

	public func image(from cgImage : CGImage, pointSize : CGSize) -> NSImage
	{
		let imageRep = NSBitmapImageRep(cgImage:cgImage)
		imageRep.size = pointSize
		let image = NSImage(size:pointSize)
		image.addRepresentation(imageRep)
		return image
	}

	public func iconImage<S : Shape>(for shape : S, scaleFactor : CGFloat, forHiDPI : Bool) -> NSImage?
	{
		let blackColor = CGColor(gray:0.0, alpha: 1.0)
		guard let cgImage = newImage(from: shape, backgroundColor: nil, strokeColor: blackColor, fillColor: blackColor, scaleFactor: scaleFactor, forHiDPI:forHiDPI) else { return nil }
		let scale = forHiDPI ? 2 : 1
		let pointSize = CGSize(width: cgImage.width/scale, height: cgImage.height/scale)
		return image(from: cgImage, pointSize:pointSize)
	}

}
