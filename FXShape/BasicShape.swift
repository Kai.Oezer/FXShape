/**
This file is part of the FXShape project.
Copyright (C) 2021 Kai Berk Oezer
https://gitlab.com/Kai.Oezer/FXShape

FXShape is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import Foundation

class BasicShape : Shape
{
	private(set) var paths : [Path]
	private(set) var circles : [Circle]
	private(set) var connectionPoints : [ConnectionPoint]
	private(set) var size : CGSize
	var attributes : [String : String]

	var doesOwnDrawing: Bool { false }
	var isReusable: Bool { true }

	var id : String
	{
		let pathsID = paths.map{ $0.id }.joined(separator: " ")
		let circlesID = circles.map{ $0.id }.joined(separator:" ")
		let connectionsID = connectionPoints.map{ $0.id }.joined(separator:" ")
		return "PA " + pathsID + "CI " + circlesID + "CO " + connectionsID
	}

	/// Designated initializer.
	///
	/// - parameters:
	///   - paths: paths in the shape
	///   - circles: the circles in the shape
	///   - connectionPoints: the connection points of the shape
	///   - size: the dimensions of the bounding box of the shape
	init(paths : [Path], circles : [Circle], connectionPoints : [ConnectionPoint], size : CGSize)
	{
		self.paths = paths
		self.circles = circles
		self.connectionPoints = connectionPoints
		self.attributes = [String:String]()
		self.size = size
	}

	enum BasicShapeCodingKey : String, CodingKey
	{
		case paths = "paths"
		case circles = "circles"
		case connectionPoints = "connectionPoints"
		case attributes = "attributes"
		case size = "size"
	}

	required init(from decoder: Decoder) throws
	{
		let container = try decoder.container(keyedBy: BasicShapeCodingKey.self)
		self.paths = try container.decode([Path].self, forKey:.paths)
		self.circles = try container.decode([Circle].self, forKey:.circles)
		self.connectionPoints = try container.decode([ConnectionPoint].self, forKey: .connectionPoints)
		self.size = try container.decode(CGSize.self, forKey: .size)
		self.attributes = try container.decode([String:String].self, forKey: .attributes)
	}

	func encode(to encoder: Encoder) throws
	{
		var container = encoder.container(keyedBy: BasicShapeCodingKey.self)
		try container.encode(self.paths, forKey: .paths)
		try container.encode(self.circles, forKey: .circles)
		try container.encode(self.connectionPoints, forKey: .connectionPoints)
		try container.encode(self.attributes, forKey: .attributes)
		try container.encode(self.size, forKey: .size)
	}

	func draw(context: ShapeRenderContext)
	{
		// ignored
	}
}
