/**
This file is part of the FXShape project.
Copyright (C) 2021-2024 Kai Berk Oezer
https://gitlab.com/Kai.Oezer/FXShape

FXShape is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import Testing
@testable import FXShape

let eps : Double = 1e-7

@Suite("Shape Tests")
struct FXShapeTests
{
	@Test("line parsing")
	func LineParsing() throws
	{
		let testPath = Path(data:"l 3 5")
		let segment = testPath.segments[0]
		if case .line(let lineSegment) = segment {
			#expect( (lineSegment.destination.x - 3.0) < eps )
			#expect( (lineSegment.destination.y - 5.0) < eps )
		} else {
			#expect(Bool(false))
		}
	}

	@Test("move-to followed by coordinate pairs")
	func movetoFollowedByCoordinatePairs() throws
	{
		// According to the SVG specification, subsequent coordinates must be treated as line-to commands.
		let testPath = Path(data:"M 3 4 6 0 11 2")
		#expect(testPath.segments.count == 3)
		if case .jump(_) = testPath.segments[0] {
		} else { #expect(Bool(false)); return }
		if case .line(let lineSegment) = testPath.segments[1] {
			#expect(abs(lineSegment.destination.x - 6.0) < eps)
			#expect(abs(lineSegment.destination.y - 0.0) < eps)
		} else { #expect(Bool(false)); return }
		if case .line(let lineSegment2) = testPath.segments[2] {
			#expect(abs(lineSegment2.destination.x - 11.0) < eps)
			#expect(abs(lineSegment2.destination.y - 2.0) < eps)
		} else { #expect(Bool(false)) }
	}

	@Test("arc parsing")
	func arcParsing() throws
	{
		let testPath = Path(data:"M 0 -16 v 6 A 8 9 1.45 0 0 0 15 v 6")
		#expect(testPath.segments.count == 4)
		if case .arc(let arcSegment) = testPath.segments[2] {
			#expect(!arcSegment.isRelative)
			#expect( abs(arcSegment.endPoint.x - 0) < eps )
			#expect( abs(arcSegment.endPoint.y - 15) < eps )
			#expect( abs(arcSegment.radiusX - 8.0) < eps )
			#expect( abs(arcSegment.radiusY - 9.0) < eps )
			#expect( abs(arcSegment.rotation - 1.45) < eps )
			#expect( !arcSegment.largeArc )
			#expect( !arcSegment.sweepPositive )
		} else { #expect(Bool(false)) }
	}

	@Test("curve parsing")
	func curveParsing() throws
	{
		let testPath = Path(data:"M 0 -16 c 4 6 7 6 10 0")
		#expect(testPath.segments.count == 2)
		if case .curve(let curveSegment) = testPath.segments[1] {
			#expect(abs(curveSegment.controlPoint1.x - 4.0) < eps)
			#expect(abs(curveSegment.controlPoint1.y - 6.0) < eps)
			#expect(abs(curveSegment.controlPoint2.x - 7.0) < eps)
			#expect(abs(curveSegment.controlPoint2.y - 6.0) < eps)
			#expect(abs(curveSegment.endPoint.x - 10.0) < eps)
			#expect(abs(curveSegment.endPoint.y) < eps)
		} else { #expect(Bool(false)) }
	}

	@Test("recognizing segment type")
	func segmentTypeRecognition() throws
	{
		let path = Path(data:"m 10 10 v 10 m 20 10 h 20 a 25,25 -30 0,1 50,-25 L 50,-25 C 100,100 250,100 250,200")
		#expect( path.segments.count == 7 )
		if case .jump(_) = path.segments[0] { } else { #expect(Bool(false)) }
		if case .verticalLine(_) = path.segments[1] { } else { #expect(Bool(false)) }
		if case .jump(_) = path.segments[2] { } else { #expect(Bool(false)) }
		if case .horizontalLine(_) = path.segments[3] { } else { #expect(Bool(false)) }
		if case .arc(_) = path.segments[4] { } else { #expect(Bool(false)) }
		if case .line(_) = path.segments[5] { } else { #expect(Bool(false)) }
		if case .curve(_) = path.segments[6] { } else { #expect(Bool(false)) }
	}

	@Test("recognizing path starts with relative move")
	func recognizingThatPathStartsWithRelativeMove() throws
	{
		let testPath = Path(data:"m 10 10 L 20 0")
		if case .line(let lineSegment) = testPath.segments[1] {
			#expect( !lineSegment.isRelative )
		} else { #expect((Bool(false))) }
	}

	@Test("recognizing path closures")
	func recognizingPathClosures() throws
	{
		let testPath1 = Path(data:"M0,20L5,0zv5")
		let testPath2 = Path(data:"M0,40c 0 1 0 3 5 5 z")
		#expect( !testPath1.closed )
		#expect( testPath2.closed )
	}
}
